<?php

return [
    'driver' => env('SYSLOG_DRIVER', 'mongodb'), //database config

    'connection' => env('SYSLOG_CONNECTION', 'mongodb'), //queue config

    'queue' => env('SYSLOG_QUEUE', 'default'), //queue name

    'chunk' => (int) env('SYSLOG_CHUNK', 1000),

    'log-request' => env('SYSLOG_REQUEST', false),

    'log-memory' => env('SYSLOG_MEMORY', true),

    'log-session' => env('SYSLOG_SESSION', true),

    'log-headers' => env('SYSLOG_HEADERS', true),

    'excepts' => [
        'telescope*',
    ],
];
